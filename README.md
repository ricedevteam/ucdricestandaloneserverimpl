# README #

This project is the UC Davis specific implementation of the Kuali Rice Standalone Server. 
This module contains UC Davis Specific modifications to the baseline Rice Foundation code which would be expected to be ported back to the Rice Foundation code at some point in the future. 

* Version 2.3.6