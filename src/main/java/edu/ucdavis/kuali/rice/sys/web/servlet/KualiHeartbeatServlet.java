package edu.ucdavis.kuali.rice.sys.web.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.kuali.rice.kew.api.KewApiServiceLocator;
import org.kuali.rice.kew.api.doctype.DocumentType;
import org.kuali.rice.kim.api.group.Group;
import org.kuali.rice.kim.api.services.KimApiServiceLocator;

public class KualiHeartbeatServlet extends HttpServlet {
    private static final org.apache.log4j.Logger LOG = org.apache.log4j.Logger.getLogger(KualiHeartbeatServlet.class);
	private static final long serialVersionUID = 4901222949286730892L;

	public void doGet(HttpServletRequest req, HttpServletResponse resp){
		this.doPost(req, resp);
	}
	public void doPost(HttpServletRequest req, HttpServletResponse resp){
        StringBuilder sb = new StringBuilder(200);
        boolean errors = false;
        sb.append("<html><head><title>heartbeat</title></head><body>");
		try {
		    // force a call to KIM
			Group g = KimApiServiceLocator.getGroupService().getGroupByNamespaceCodeAndName("KR-WKFLW","WorkflowAdmin");
	        if ( g == null ) {
	            sb.append( "ERROR: NO GROUPS RETRIEVED");
	            errors = true;
	        }
	        // attempt a workflow call
	        DocumentType docType = KewApiServiceLocator.getDocumentTypeService().getDocumentTypeByName("KualiDocument");
	        if ( docType == null  ) {
	            sb.append( "POTENTIAL PROBLEM: KualiDocument document type not found" );
	            errors = true;
	        }
	        if ( !errors ) {
	            sb.append( "LUB-DUB,LUB-DUB");
	        }
		} catch ( Exception ex ){
		    sb.append( "Exception running heartbeat monitor: " + ex.getClass() + ": " + ex.getMessage() );
		    StringWriter sw = new StringWriter(1000);
		    PrintWriter pw = new PrintWriter( sw );
		    ex.printStackTrace( pw );
		    sb.append( sw.toString() );
		    LOG.fatal( "Failed to detect heartbeat.  Apply paddles stat!   beeeeeeeeeeeeeeeeeeeeeeeeeeep  He's dead Jim.", ex);
		} finally {
		    sb.append( "</body></html>");
    		try {
    			resp.getWriter().println(sb.toString());
    			resp.addHeader("Content-Type", "text/html");
    		} catch (IOException ex) {
                LOG.error( "Failure writing to heartbeat servlet output stream:", ex );
    		}
		}
	}
}
