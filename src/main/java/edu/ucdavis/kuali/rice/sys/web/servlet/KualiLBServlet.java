package edu.ucdavis.kuali.rice.sys.web.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.kuali.rice.ksb.service.KSBServiceLocator;

public class KualiLBServlet extends HttpServlet {
    private static final org.apache.log4j.Logger LOG = org.apache.log4j.Logger.getLogger(KualiLBServlet.class);
	private static final long serialVersionUID = 4901222949286730892L;

	public void doGet(HttpServletRequest req, HttpServletResponse resp)
	{
		this.doPost(req, resp);
	}

	public void doPost(HttpServletRequest req, HttpServletResponse resp)
	{
		StringBuilder sb = new StringBuilder(2048);
		sb.append("<html><head><title>Kuali LB Servlet</title></head><body>");

		DataSource dataSource = KSBServiceLocator.getRegistryDataSource();
		try {
			Connection connection = dataSource.getConnection();
			connection.close();
			sb.append("Retrieved Connection OK");
		} catch (SQLException e) {
			try {
				sb.append( "Failed to retrieve database connection: "+e.getLocalizedMessage());
				resp.sendError(HttpServletResponse.SC_NOT_FOUND);
			} catch (IOException e1) {
				LOG.error( this.getClass().getName()+" - Failed to send response: "+e1.getLocalizedMessage());
				e1.printStackTrace();
			}
		}

		sb.append( "</body></html>");
		try {
			resp.getWriter().println(sb.toString());
			resp.addHeader("Content-Type", "text/html");
		} catch (IOException ex) {
			LOG.error( "Failure writing to LB servlet output stream:", ex );
		}
	}
}
